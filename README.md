和平之翼Java代码生成器SMEU 3.2.0版，研发代号(乌篷船 Black Awning Boat)已发布，本软件是旗舰版的动词算子式代码生成器。在附件中提供war包下载。此版本支持使用Excel模板的代码生成，支持初始数据导入，支持SGS语言语法加亮。欢迎使用。

本代码生成器是超级语言（SGS 标准生成器脚本）驱动的先进编译系统。旨在演示数据驱动的代码生成器固有的生产率上的优势和与标准编译器（Java语言）的良好协作关系。在未来，更先进的代码生成器和编译器的组合会显现出巨大的生产力优势，让我们一起促成这一天所需要的技术的进化循环。

乌篷船支持华丽的Excel模板代码生成和初始数据导入，软件新增Excel生成界面和示例Excel模板。界面和Excel模板如下图。

此分支为无垠式/和平之翼代码生成器阵列三大旗舰分支之一,其他两支为无垠式代码生成器JEEEU版和和平之翼代码生成器SHCEU版。

目前和平之翼代码生成器SMEU 3.2.0版乌篷船正式版已发布, 此版本支持Service，Dao组件扫描，SGS初始数据导入，多对多初始数据导入，一对多动态标签等先进特性，新增在线文档：代码生成器技术乱弹，欢迎使用。现在此版本war包已上传至本站附件栏，并经过详细测试，有重大Bug修复与功能更新，涉及一系列遗漏的Bug修复，包括Excel模板生成的一些缺陷，和Boolean类型的一系列缺陷，已发现的Bug均已修复，欢迎下载使用：  [https://gitee.com/jerryshensjf/PeaceWingSMEU/attach_files](https://gitee.com/jerryshensjf/PeaceWingSMEU/attach_files)

3.2.0正式版源码下载请下载3.2.0的发行版，请至：[https://gitee.com/jerryshensjf/PeaceWingSMEU/releases/RELEASE_3_2_0](https://gitee.com/jerryshensjf/PeaceWingSMEU/releases/RELEASE_3_2_0)

本站附件中有使本生成器支持MySQL 8.0的补丁，感谢QQ好友100100101的贡献，下载请至：
[https://gitee.com/jerryshensjf/PeaceWingSMEU/attach_files](http://[输入链接说明](http://))

乌篷船：

![输入图片说明](https://gitee.com/uploads/images/2018/0206/144737_58e25407_1203742.jpeg "wpc2.jpg")附件

Excel模板：
![输入图片说明](https://gitee.com/uploads/images/2017/1205/140634_26b1560b_1203742.png "exceltemplate_1.png")

![输入图片说明](https://gitee.com/uploads/images/2017/1205/140649_e5780e3d_1203742.png "exceltemplate_2.png")

Excel生成界面：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1110/203605_87606acc_1203742.png "excel.png")

传统的SGS（标准生成器脚本）生成界面，支持SGS语法加亮：
![输入图片说明](https://gitee.com/uploads/images/2018/0313/210936_9d663d5f_1203742.png "bab.png")

代码生成物截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1110/203711_26936605_1203742.png "grid2.png")

代码生成物多对多界面截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1110/203721_9c0a34b4_1203742.png "mtm2.png")

代码生成物下拉列表截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1110/203736_6067f6bd_1203742.png "dropdown2.png")

代码生成物更新界面截图：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1110/203825_a6319c55_1203742.png "update2.png")

最新稳定版本和平之翼代码生成器SMEU 3.2.0 正式版。

和平之翼代码生成器SMEU版，一键支持下拉列表和多对多，已支持Oracle数据库。

SMEU技术栈支持JQuery Easy UI,Spring MVC4,spring4, MyBatis 3。

本版支持下拉列表，使用者只需要在域对象相应的外键字段设定dropdown:DomainName fieldName;
即下拉列表：外键域名 字段名，即可一键支持下拉列表（外键）。

本版支持多对多关系，只要在多对多关系的主域对象中定义了
manytomanyslave:slaveDomainName即可在生成的功能和数据库定义中支持了两者的多对多
关系。

和平之翼代码生成器是动词算子式Java通用代码生成器，是无垠式代码生成器的第二代。
修正了一些以前的Bug，并作了功能增强和文档更新。希望您喜欢。
支持Oracle数据库，您只需要定义dbtype:oracle即可支持Oracle数据库，详细情况请看相关示例。

和平之翼代码生成器SMEU版4.0宝船(Treasure Ship)的开发将于近期在主干版本启动。

宝船：

![输入图片说明](https://images.gitee.com/uploads/images/2018/0708/224107_74d3ac15_1203742.jpeg "TreasureShip.jpg")

和平之翼代码生成器图标，翅膀：

![输入图片说明](https://images.gitee.com/uploads/images/2018/1105/161512_b814baaa_1203742.jpeg "peacewing.jpg")