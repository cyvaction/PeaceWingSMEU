1.parent_id 为 null时生成的SQL脚本里为 ''
2.需要测试Excel模板生成Oracle项目
3.需要测试11条数据的分页
4.如果测试数据多最后一条不出现的bug
5.boolean普通字段Add,Update动词，值始终为false【已修复,已测试】
6.Oracle示例中对象并未按Id升序排列【已修复,已测试】
7.Oracle示例未支持boolean字面量【已修复,已测试】
8.Oracle示例Grid弹性万能界面仍然是英文页眉页脚【已修复,已测试】
9.级联删除问题：最好提示，不可删除。目前解决办法，外连接时置入空字符串【已修复】
10.SGS GenerateAdvanced示例，编辑时deleted为False，checkbox却为True 【已修复】
11.Excel GenerateSample示例，搜索active=“false”时，结果集错误【已修复,已测试】
12.Excel GenerateSample示例，用户名的中文标签为角色名称【已修复】