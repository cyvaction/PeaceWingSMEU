1.默认数据库名问题【已修复】
2.BooleanUtil (java)【已修复】
3.parseBoolean(js)【已修复】
4.基础类型用大写类【已修复】
5.域对象类名的链接，应该使用getText【已修复】
6.面板题名的null【已修复】
7.Grid脚注的NaN【已修复】
8.生成器界面【已修复】
9.几个新动词调通【已修复】
10.有几个新的Count动词【已修复】
11.update的Boolean类型字段传值不对【已修复】
12.生成物使用类类型【已修复】
13.生成器中文支持【已修复】
14.技术栈改名SMEU【已修复】
15.Compile Error 和Compile Warning修复【已修复】
16.支持用类类型声明字段【已修复】
17.js parseBoolean【已修复】
18.js isBlank【已修复】
19.GenerateAdvanced Integer.Integer.parseInt issue【已修复】
20.EU Homepage中英文兼顾【已修复】
21.dropdown的Label
22.活跃字段为deleted时，标签应该是删除【已修复】
23.新建数据表时要加上删除联系表的语句【已修复】
24.示例增强，加上所用的Dropdown
26.多对多管理列出似乎有问题，是Boolean值问题【GenerateOracleAdvanced已修复】
27.域对象要实现comparable接口【GenerateOracleAdvanced已修复】
28.GenerateOracleAdvanced示例Assign有问题，类型转换有问题，编译错【已修复】
29.GenerateOracle示例复杂动词的关系ID有类型问题[目视已修复，待测试]
30.分页参数应该用数字位置参数而不是文字参数【已修复】
31.dropdown在Excel里定义成field，会生成出错。【已修复】
32.发现别名不是domainId的，生成时会有编译错误。【其实就是35】【已修复】
33.下拉列表的label有问题，取的是对象Domain的ID的label而不是别名的label。【已修复】
34.数据导入时没有过滤掉单引号。【已修复】
35.field是大写字母开头时会有编译错误。【已修复】
36.Oracle数据库comment为关键字【现规避，并未处理】
37.Oracle数据库type为关键字【现规避，并未处理】
38.Oracle数据库password为关键字【现规避，并未处理】
39.Oracle数据库level为关键字【现规避，并未处理】
40.Oracle数据库description为关键字【现规避，并未处理】
41.初始化数据没有类型检查【暂未处理】
42.数据库order为关键字【现规避，并未处理】
43.Dropdown的字段名一定为domainId格式，需加校验【修复】
44.字段名第二个一定要为小写，否则系统会认为第一个字母是大写，需要校验【修复】
45.SQL关键字如Order等要加校验【修复】
46.Excel模板字段名去除空格【修复】
47.UserSystems示例Excel版User有两个字段gender需要删除一个并对域对象增加字段名重复校验【Excel已修复，校验未加】
48.MyAreas示例Excel和SGS密码不一致【修复】
49.MyAreas示例SGS与Excel不一致【修复】
50.MySports示例Excel和SGS密码不一致【修复】
51.MySports示例SGS与Excel不一致
52.SGS生成代码尚未进行Sql关键字校验
53.MyProject示例Excel和SGS密码不一致【修复】
54.MyProject SGS示例需要修改
55.Excel代码生成，第二字母小写未校验【？】