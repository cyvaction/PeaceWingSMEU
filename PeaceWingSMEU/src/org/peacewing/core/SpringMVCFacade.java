package org.peacewing.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.peacewing.domain.Domain;
import org.peacewing.domain.Facade;
import org.peacewing.domain.Field;
import org.peacewing.domain.Method;
import org.peacewing.domain.ValidateInfo;
import org.peacewing.exception.ValidateException;
import org.peacewing.utils.StringUtil;

public class SpringMVCFacade extends Facade implements Comparable<SpringMVCFacade>{
	protected List<Verb> verbs = new ArrayList<Verb>();

	public List<Verb> getVerbs() {
		return verbs;
	}

	public void setVerbs(List<Verb> verbs) {
		this.verbs = verbs;
	}

	public void addVerb(Verb verb) {
		this.verbs.add(verb);
	}

	public SpringMVCFacade(Domain domain) throws ValidateException {
		super();
		fixingDecorate(domain);
	}

	public void fixingDecorate(Domain domain){
		Field serviceField = new Field("service",domain.getStandardName()+"Service");
		serviceField.addAnnotation("Autowired");
		this.addField(serviceField);
		this.addClassImports(domain.getPackageToken()+".service."+domain.getStandardName()+"Service");
		this.addClassImports(domain.getPackageToken()+".domain."+domain.getStandardName());
		this.addClassImports("org.springframework.beans.factory.annotation.Autowired");
		this.addClassImports("org.springframework.web.bind.annotation.PathVariable");
		this.addClassImports("org.springframework.web.bind.annotation.RequestBody");
		this.addClassImports("org.springframework.web.bind.annotation.RequestMapping");
		this.addClassImports("org.springframework.web.bind.annotation.RequestMethod");
		this.addClassImports("org.springframework.web.bind.annotation.RequestParam");
		this.addClassImports("org.springframework.web.bind.annotation.RestController");
		this.addClassImports("java.util.List");
		this.addClassImports("java.util.Map");
		this.addClassImports("java.util.TreeMap");
		this.addClassAnnotation("RestController");
		this.addClassAnnotation("RequestMapping(value = \"/"+domain.getLowerFirstDomainName()+"Facade\")");
	}

	@Override
	public String generateFacadeString() throws ValidateException {
		StringBuilder sb = new StringBuilder();
		if (this.packageToken != null
				&& !"".equalsIgnoreCase(this.packageToken))
			sb.append("package ").append(this.packageToken).append(".")
					.append("facade").append(";\n\n");
		Set<String> imports = this.generateImportStrings();
		imports.addAll(this.classImports);
		for (String s : imports) {
			sb.append("import ").append(s).append(";\n");
		}
		sb.append("\n");

		for (String s : this.classAnnotations) {
			sb.append("@").append(s).append("\n");
		}
		sb.append("public class ")
				.append(this.getStandardName())
				.append(" {\n");

		// generate fields notions
		for (Field f : this.getFields()) {
			for (String annotation : f.getAnnotations()) {
				sb.append("\t@").append(annotation).append("\n");
			}
			sb.append("\tprotected ").append(f.getFieldType()).append(" ")
					.append(f.getLowerFirstFieldName());
			if (f.getFieldValue() != null) {
				if (!f.getFieldType().equals("String")){
					sb.append(" = ").append(f.getFieldValue());
				}else {
					sb.append(" = \"").append(f.getFieldValue()).append("\"");
				}
			}
			sb.append(";\n");
		}
		sb.append("\n");
		// generate getter setter notions

		/*for (Field f : this.getFields()) {
			// generate setters
			sb.append("\tpublic void set")
					.append(StringUtil.capFirst(f.getCapFirstFieldName()))
					.append("(").append(f.getFieldType()).append(" ")
					.append(StringUtil.lowerFirst(f.getLowerFirstFieldName()))
					.append("){\n");
			sb.append("\t\tthis.")
					.append(StringUtil.lowerFirst(f.getLowerFirstFieldName()))
					.append(" = ")
					.append(StringUtil.lowerFirst(f.getLowerFirstFieldName()))
					.append(";\n");
			sb.append("\t}\n\n");
			// generate getters
			sb.append("\tpublic ").append(f.getFieldType()).append(" get")
					.append(StringUtil.capFirst(f.getLowerFirstFieldName()))
					.append("(){\n");
			sb.append("\t\treturn this.")
					.append(StringUtil.lowerFirst(f.getLowerFirstFieldName()))
					.append(";\n");
			sb.append("\t}\n\n");
		}*/

		Iterator it2 = this.getMethods().iterator();
		while (it2.hasNext()) {
			sb.append(((Method) it2.next()).generateMethodString());
		}
		sb.append("}\n\n");
		return sb.toString();
	}

	public ValidateInfo validate() {
		ValidateInfo info = new ValidateInfo();
		info.setSuccess(true);
		if (this.verbs == null || this.verbs.size() == 0) {
			info.setSuccess(false);
			info.addCompileError("Facade " + this.standardName
					+ " 动词是空！");
		}
		for (Verb v : this.verbs) {
			if (v != null && v.getDomain() == null) {
				info.setSuccess(false);
				info.addCompileError("Facade " + this.standardName + "动词"
						+ v.getVerbName() + "域对象是空值！");
			}
		}
		return info;
	}

	public SpringMVCFacade(List<Verb> verbs, Domain domain)
			throws ValidateException {
		super();
		try{
			for (Verb v : verbs) {
				v.setDomain(domain);
			}
			this.verbs = verbs;
			this.standardName = domain.getCapFirstDomainName() + "Facade";
	
			ValidateInfo info = this.validate();
			if (info.success()) {
				for (Verb v : this.verbs) {
					Method verbMethods = v.generateFacadeMethod();
					this.addMethod(verbMethods);
				}
				fixingDecorate(domain);
			} else {
				ValidateException e = new ValidateException(info);
				throw e;
			}
		}catch (Exception ex){
			ValidateInfo info = new ValidateInfo();
			info.addCompileError("SpringMVC控制器新建失败！");
			ValidateException e = new ValidateException(info);
			throw e;
		}
	}

	public String getCapFirstFacadeName() {
		return StringUtil.capFirst(this.getStandardName());
	}

	public String getLowerFirstFacadeName() {
		return StringUtil.lowerFirst(this.getStandardName());
	}

	public Domain getDomain() {
		if (this.verbs != null && this.verbs.size() > 0)
			return this.verbs.get(0).getDomain();
		else
			return null;
	}

	public Method findVerbMethodByName(String methodname) throws Exception {
		for (Verb vb : this.verbs) {
			Method vbm = vb.generateFacadeMethod();
			if (vbm.getStandardName().equalsIgnoreCase(methodname))
				return vbm;
		}
		return null;
	}

	@Override
	public int compareTo(SpringMVCFacade o) {
		return this.standardName.compareTo(o.getStandardName());
	}

}
