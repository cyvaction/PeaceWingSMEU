package org.peacewing.utils;
import java.util.Iterator;

import org.apache.commons.lang3.math.NumberUtils;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Dropdown;
import org.peacewing.domain.Field;
import org.peacewing.domain.ManyToMany;
import org.peacewing.domain.Var;
import org.peacewing.exception.ValidateException;

public class SqlReflector {
	public static String generateTableDefinition(Domain domain) throws Exception{
		String result = "create table " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" (";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
	        Field f = (Field)it.next();
	        String fieldName = f.getFieldName();
	        String fieldType = f.getClassType().getTypeName();
	        result += changeDomainFieldtoTableColumDefinitionToken(domain, fieldName, fieldType)+ ",";
        }
		String  ptoken = generatePrimaryKeySqlToken(domain);
		if (ptoken.length() >0 ){
			result += ptoken;
		}else {
			result = result.substring(0,result.length()-1);
		}
        result += ");";
		return result;
	}
	
	public static String generateLinkTableDefinition(Domain master, Domain slave) throws Exception{
		String result = "create table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+slave.getStandardName()) +" (";
		result += StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+"Id") +" "+lookupSqlType(master.getDomainId().getFieldType()) + " not null, ";
		result += StringUtil.changeDomainFieldtoTableColum(slave.getStandardName()+"Id") +" "+lookupSqlType(slave.getDomainId().getFieldType()) + " not null ";
        result += ");";
		return result;
	}

	public static String generateInsertSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "insert into " + domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" ";
		Iterator it = domain.getFieldsWithoutId().iterator();
		
		result += "( ";
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        result += StringUtil.changeDomainFieldtoTableColum(fieldName)+ ",";
        }
        result = result.substring(0,result.length()-1);
        result += ") values (";
		Iterator it2 = domain.getFieldsWithoutId().iterator();
        while (it2.hasNext()) {	
        	Field f = (Field)it2.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        result += "?,";
        }
        result = result.substring(0,result.length()-1);
        result += ");";
        return result;
	}
	
	public static String generateUpdateSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "update " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" set ";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        if (!isPrimaryKey(domain, fieldName, fieldType)) {
	        	result += StringUtil.changeDomainFieldtoTableColum(fieldName)+ " = ? ,";
	        }
        }
        result = result.substring(0,result.length()-1);
        result += generatePrimaryWhereParamSqlToken(domain);
        result += ";";
        return result;
	}
	
	public static String generateDeleteSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "delete from " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) + " ";
        result += generatePrimaryWhereParamSqlToken(domain);
        result += ";";
        return result;
	}
	
	public static String generateSoftDeleteSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "update " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) + " set " + domain.getActive().getFieldName() + " = false where ";
		result += StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName())+ " = ? ";
        result += ";";
        return result;
	}
	
	public static String changeDomainFieldtoTableColumDefinitionToken(Domain domain, String fieldName, String fieldType){
		String result = StringUtil.changeDomainFieldtoTableColum(fieldName) + " ";
		result += lookupSqlType(fieldName,fieldType) + " ";
		if (isPrimaryKey(domain,fieldName,fieldType)){
			result += "not null auto_increment";
		}else if (isDoaminIdOrActiveField(domain,fieldName,fieldType)){
			result += "not null ";
		}else {
			result += "null ";
		}
		return result;
	}
	
	public static boolean isPrimaryKey(Domain domain,String fieldName,String fieldType){
		boolean retVal = false;
		if ("long".equalsIgnoreCase(fieldType)||"int".equalsIgnoreCase(fieldType)||"Integer".equalsIgnoreCase(fieldType)){
			if ("id".equalsIgnoreCase(fieldName)||fieldName.equalsIgnoreCase(domain.getStandardName()+"id")||fieldName.equalsIgnoreCase(domain.getDomainId().getFieldName())){
				retVal =true;
			}
		}
		return retVal;
	}
	
	public static boolean isDoaminIdOrActiveField(Domain domain,String fieldName,String fieldType){
		if ((fieldType.equalsIgnoreCase("boolean")&&fieldName.equals(domain.getActive().getFieldName()))||(fieldType.equalsIgnoreCase("string")&&fieldName.equals(domain.getDomainName().getFieldName()))){
			return true;
		}
		return false;
	}
	
	public static String generatePrimaryKeySqlToken(Domain domain){
		String result = "";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        if (isPrimaryKey(domain, fieldName, fieldType)){
	        	result =  "primary key (" + StringUtil.changeDomainFieldtoTableColum(fieldName) +")";
	        }
        }
        return result;
	}
	
	public static String generatePrimaryWhereParamSqlToken(Domain domain){
		String result = "";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        if (isPrimaryKey(domain, fieldName, fieldType)){
	        	result = "where " +  StringUtil.changeDomainFieldtoTableColum(fieldName) +" = ?";
	        }
        }
        return result;
	}
	
	public static String lookupSqlType(String fieldName, String fieldType){
		String result = "";
		if (fieldType.equalsIgnoreCase("long")) {
			result = "BigInt";
		}
		if  (fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double")) {
			result = "Double";
		}
		if  (fieldType.equalsIgnoreCase("BigDecimal")||fieldType.equalsIgnoreCase("decimal")) {
			result = "Decimal";
		}
		if  (fieldType.equalsIgnoreCase("boolean")) {
			result = "bool";
		}
		if  (fieldType.equalsIgnoreCase("String")) {
			if (fieldName.toLowerCase().contains("comment")||fieldName.toLowerCase().contains("description")||fieldName.toLowerCase().contains("content")){
				result = "text";
			}else {
				result = "varchar(255)";
			}
		}
		return result; 
	}
	
	public static String lookupSqlType(String fieldType){
		String result = "";
		if (fieldType.equalsIgnoreCase("long")) {
			result = "BigInt";
		}
		if  (fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double")) {
			result = "Double";
		}
		if  (fieldType.equalsIgnoreCase("BigDecimal")||fieldType.equalsIgnoreCase("decimal")) {
			result = "Decimal";
		}
		if  (fieldType.equalsIgnoreCase("boolean")) {
			result = "bool";
		}
		if  (fieldType.equalsIgnoreCase("String")) {
			result = "varchar(255)";
		}
		return result; 
	}
	
	public static String generateSelectAllStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +";";
        return result;
	}
	
	public static String generateSelectAllByPageStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" limit ?,?;";
        return result;
	}
	
	public static String generateCountRecordStatement(Domain domain, Var countNum) throws Exception{
		String result = "select count("+ StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName()) + ") as "+countNum.getVarName()+" from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +";";
        return result;
	}
	
	public static String generateSelectByFieldStatement(Domain domain, Field field) throws Exception{
		String result = "select "+  DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" where "+ StringUtil.changeDomainFieldtoTableColum(field.getFieldName())+" = ?;";
        return result;
	}
	
	public static String generateSelectActiveStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getActive().getFieldName())+" = true;";
        return result;
	}
	
	public static String generateFindByIdStatement(Domain domain) throws Exception{
		String result = "select "+DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName())+" = ?;";
        return result;
	}
	
	public static String generateFindByNameStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getDomainName().getFieldName())+" = ?;";
        return result;
	}
	
	public static String generateSearchByNameStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getDomainName().getFieldName())+" like ?";
        return result;
	}
	
	public static String generateSearchByDescriptionStatement(Domain domain, Field description) throws Exception{
		String result = "select " +DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(description.getFieldName())+" like %?%;";
        return result;
	}

	public static String generateDropTableStatement(Domain domain) throws Exception{
		String result = "DROP TABLE "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain)+" if exists;";
		return result;
	}
	
	public static String generateInsertSqlWithValue(Domain domain) throws Exception{
		String result = "insert into " + domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" ";
		Iterator it = domain.getFields().iterator();
		
		result += "( ";
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
 	        if (f instanceof Dropdown) result += StringUtil.changeDomainFieldtoTableColum(((Dropdown)f).getAliasName())+ ",";
 	        else result += StringUtil.changeDomainFieldtoTableColum(fieldName)+ ",";
        }
        result = result.substring(0,result.length()-1);
        result += ") values (";
		Iterator it2 = domain.getFields().iterator();
        while (it2.hasNext()) {	
        	Field f = (Field)it2.next();
        	String fieldValue = "";
			String fieldName = "";
			String fieldType = "";
        	if (f instanceof Dropdown) {
        		Dropdown dp = (Dropdown)f;
        		fieldValue = dp.getFieldValue();
     	        fieldName = dp.getAliasName();
     	        fieldType = dp.getFieldType();
        	}else {
     	        fieldValue = f.getFieldValue();
     	        fieldName = f.getFieldName();
     	        fieldType = f.getFieldType();
        	}        	

 	        try {
	 	        if (!StringUtil.isBlank(fieldValue)&&NumberUtils.isNumber(fieldValue)){
	 	        	if (Double.valueOf(fieldValue) - Math.round(Double.valueOf(fieldValue)) < 0.00005){
	 	        		fieldValue = "" + Math.round(Double.valueOf(fieldValue));
	 	        		System.out.println("JerryDebug:roundtoint:"+fieldValue);
	 	        	}
	 	        }
 	        } catch (Exception e){
 	        	e.printStackTrace();
 	        	throw new ValidateException("数据项"+fieldName+"类型定义错误！");
 	        }
 	       if (!StringUtil.isBlank(fieldType)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double")||fieldType.equalsIgnoreCase("boolean"))) {
 	    	   if (StringUtil.isBlank(fieldValue)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double"))) result += "0,";
 	    	   else result +=  fieldValue + ",";
	        } else {
	        	result += "'"+ fieldValue + "',";
	        }
        }
        result = result.substring(0,result.length()-1);
        result += ");";
        return result;
	}
	
	public static String generateMtmInsertSqlWithValues(ManyToMany mtm) throws Exception{
		if (StringUtil.isBlank(mtm.getValues())) return "";
		else {
		String [] slaveValues = mtm.getValues().split(",");
			String result = "";
			for (String slaveValue:slaveValues) {
			    result = result + "insert into " +  mtm.getMaster().getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(mtm.getMaster().getStandardName()+mtm.getSlave().getStandardName()) +" ";
				result = result + "("+StringUtil.changeDomainFieldtoTableColum(mtm.getMaster().getStandardName()+"Id")+" , "+StringUtil.changeDomainFieldtoTableColum(mtm.getSlave().getStandardName()+"Id")+") values ( ";		
		        if (TypeUtil.isNumeric(mtm.getMaster().getDomainId().getFieldRawType())){
		        	result = result + mtm.getMaster().getDomainId().getFieldValue() + "," ;
		        }else {
		        	result = result +"'"+ mtm.getMaster().getDomainId().getFieldValue() + "',"; 
		        }
		        if (TypeUtil.isNumeric(mtm.getSlave().getDomainId().getFieldRawType())){
		        	result = result + slaveValue ;
		        }else {
		        	result = result +"'"+ slaveValue + "'"; 
		        }
		        result += ");\n";
			}
			return result;
		}
	}
}
