package org.peacewing.utils;

import org.peacewing.domain.Domain;

public class TableStringUtil {
	public static String domainNametoTableName(Domain domain) throws Exception {
		return StringUtil.changeDomainFieldtoTableColum(domain.getPlural());
	}
	
	public static String domainNametoCapTableName(Domain domain) throws Exception {
		return (domain.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(domain.getPlural())).toUpperCase();
	}
	
	public static String domainNametoLowerTableName(Domain domain) throws Exception {
		return (domain.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(domain.getPlural())).toLowerCase();
	}
	
	public static String domainNametoTableNameWithDbPrefix(Domain domain) throws Exception {
		return domain.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(domain.getPlural());
	}
	
	public static String twoDomainNametoTableNameWithDbPrefix(Domain master,Domain slave) throws Exception {
		return master.getDbPrefix()+StringUtil.changeDomainFieldtoTableColum(master.getCapFirstDomainName())+"_"+StringUtil.changeDomainFieldtoTableColum(slave.getCapFirstDomainName());
	}

	public static String domainIdNametoTableFieldName(Domain domain) {
		return StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName()).toLowerCase();
	}
}
