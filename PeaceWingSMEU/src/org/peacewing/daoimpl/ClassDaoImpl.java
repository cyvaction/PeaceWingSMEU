package org.peacewing.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.peacewing.dao.ClassDao;
import org.peacewing.database.DBConf;
import org.peacewing.domain.Naming;
import org.peacewing.domain.Class;

public class ClassDaoImpl implements ClassDao {

	@Override
	public String generateClassString(Naming naming, String standardName)
			throws Exception {
		Class clazz = this.generateClass(naming, standardName);
		return clazz.generateClassString();
	}
	
	@Override
	public Class generateClass(Naming naming, String standardName)
			throws Exception {
		Connection connection = DBConf.initDB();
        String query = "SELECT * FROM domain,domain_fields,naming " +
        		"where domain.domain_id = domain_fields.domain_id " + 
        		"and naming.naming_id=domain.naming_id " +
        		"and naming.naming_id = ? " +
        		"and domain.domain_name=? ";
        
        PreparedStatement ps = connection.prepareStatement(query);
        
        ps.setLong(1,naming.getNamingid());
        ps.setString(2, standardName);
        Class domain = new Class();
        ResultSet result = ps.executeQuery();

        while(result.next()) {
   	
        }
        DBConf.closeDB(connection);     
		return null;
	}

}
