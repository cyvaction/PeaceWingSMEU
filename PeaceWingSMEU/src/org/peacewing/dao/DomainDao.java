package org.peacewing.dao;

import org.peacewing.domain.Naming;

public interface DomainDao {
public String generateDomainString(Naming naming, String standardName) throws Exception;
public String generateDomainString(long namingid);
}
