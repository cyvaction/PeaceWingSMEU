package org.peacewing.dao;

import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Interface;

public interface DaoDao {
	public Interface generateDao(Domain domain, List<Verb> verbs) throws Exception;
	public Class generateDaoImpl(Domain domain, List<Verb> verbs) throws Exception;	
}
