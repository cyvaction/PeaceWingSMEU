package org.peacewing.generator;

import org.peacewing.domain.Domain;

public class JspTemplateFactory {
	public static JspTemplate getInstance(String type,Domain domain){
		switch (type) {
			case "grid" : return new GridJspTemplate(domain);
			case "pagingGrid" : return new PagingGridJspTemplate(domain);
			case "onlyloginindex":
						  return new OnlyLoginIndexJspTemplate();
			case "jsonPagingGrid": return new JsonPagingGridJspTemplate(domain);
			default		: return null;
		}
	}
}
