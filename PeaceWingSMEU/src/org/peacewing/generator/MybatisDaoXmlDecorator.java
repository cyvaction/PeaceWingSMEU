package org.peacewing.generator;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;
import org.peacewing.domain.Method;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.utils.WriteableUtil;

public class MybatisDaoXmlDecorator {
	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public List<Domain> getResultMaps() {
		return resultMaps;
	}

	public void setResultMaps(List<Domain> resultMaps) {
		this.resultMaps.addAll(resultMaps);
	}

	public List<Method> getDaoXmlMethods() {
		return daoXmlMethods;
	}

	public void setDaoXmlMethods(List<Method> daoXmlMethods) {
		this.daoXmlMethods.addAll(daoXmlMethods);
	}
	public void addDaoXmlMethod(Method method) {
		this.daoXmlMethods.add(method);
	}
	public void addResultMap(Domain domain) {
		this.resultMaps.add(domain);
	}
	protected Domain domain;
	protected List<Domain> resultMaps = new ArrayList<Domain>();
	protected List<Method> daoXmlMethods = new ArrayList<Method>();

	public StatementList generateDaoXmlResultMaps(long serial,int indent,Domain domain){
		StatementList result = new StatementList();
		result.add(new Statement(serial,indent,"<resultMap id=\""+domain.getLowerFirstDomainName()+"\" type=\""+domain.getFullName()+"\">"));
		result.add(new Statement(serial+100L,indent+1,"<id column=\""+domain.getDomainId().getTableColumnName()+"\" property=\""+domain.getDomainId().getLowerFirstFieldName()+"\"/>"));
		int fieldSize = domain.getFieldsWithoutId().size();
		Field[] fields = domain.getFieldsWithoutId().toArray(new Field[fieldSize]);		
		for (int i=0;i<fieldSize;i++){
			Field f = fields[i];
			result.add(new Statement(serial+200L+100L*i,indent+1,"<result column=\""+f.getTableColumnName()+"\" property=\""+f.getLowerFirstFieldName()+"\"/>"));		
		}
		result.add(new Statement(serial+300L+100L*fieldSize,indent,"</resultMap>"));
		result.add(new Statement(serial+300L+100L*fieldSize+100L,indent,""));
		result.setSerial(serial);
		result.setIndent(indent);
		return result;
	}
	
	public String generateMybatisDaoXmlFileStr(){
		List<Writeable> result = new ArrayList<Writeable>();
		result.add(new Statement(100L,0,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
		result.add(new Statement(200L,0,"<!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\" \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">"));
		result.add(new Statement(300L,0,"<mapper namespace=\""+this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao"+"\"> "));
		
		int resultMapsize = this.resultMaps.size();
		for (int i=0;i<resultMapsize;i++){
			result.add(generateDaoXmlResultMaps(400L+100L*i,1,this.resultMaps.get(i)));
		}
		
		int daoXmlMehtodSize = this.daoXmlMethods.size();
		for (int i=0;i<daoXmlMehtodSize;i++){
			Method m = this.daoXmlMethods.get(i);
			if (m!= null) {
				m.setNoContainer(true);
				m.setSerial(400L+100L*resultMapsize+100L*i);
				result.add(m.getMethodStatementList());
				result.add(new Statement(400L+100L*resultMapsize+100L*i+50L,0,""));
			}			
		}
		
		result.add(new Statement(400L+100L*(resultMapsize+daoXmlMehtodSize),0,"</mapper>"));
		return WriteableUtil.merge(result).getContent();
	}
}
