package org.peacewing.verb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.core.VerbFactory;
import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.generator.NamedStatementListGenerator;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class SoftDeleteAll extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception  {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() {
		Method method = new Method();
		method.setStandardName("softDeleteAll"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,InterVarUtil.DB.ids.getVarName(),InterVarUtil.DB.ids.getVarType()));
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() {
		return null;
	}

	@Override
	public String generateControllerMethodString() {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() {
		Method method = new Method();
		method.setStandardName("softDeleteAll"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,"ids",new Type("String")));
		method.addMetaData("Override");
		
		//Service method
		Method servicemethod = new Method();
		servicemethod.setStandardName("softDelete"+StringUtil.capFirst(this.domain.getStandardName()));
		servicemethod.setReturnType(new Type("void"));
		servicemethod.setThrowException(true);
		servicemethod.addSignature(new Signature(1,this.domain.getDomainId().getLowerFirstFieldName(),this.domain.getDomainId().getRawType()));
						
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,2,"String [] idArr = ids.split(\",\");"));
		list.add(new Statement(2000L,2,"for (String idString : idArr){"));
		if (this.domain.getDomainId().getRawType().isLong()){
			list.add(new Statement(3000L,3,this.domain.getDomainId().getClassType() +" "+this.domain.getDomainId().getLowerFirstFieldName()+" = Long.valueOf(idString);"));
		} else if (this.domain.getDomainId().getRawType().isInt()){
			list.add(new Statement(3000L,3,this.domain.getDomainId().getClassType() +" "+this.domain.getDomainId().getLowerFirstFieldName()+" = Integer.valueOf(idString);"));
		}		
		list.add(NamedStatementListGenerator.generateServiceImplVoid(4000L, 3, InterVarUtil.DB.dao, servicemethod));
		list.add(new Statement(5000L,2,"}"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() {
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public SoftDeleteAll(){
		super();
		this.setLabel("批软删除");
	}	
	
	public SoftDeleteAll(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("SoftDeleteAll"+StringUtil.capFirst(this.domain.getPlural()));
		this.setLabel("批软删除");
	}

	@Override
	public String generateControllerMethodStringWithSerial() {
		return null;
	}

	@Override
	public Method generateFacadeMethod() {
		Method method = new Method();
		method.setStandardName("softDeleteAll"+StringUtil.capFirst(this.domain.getPlural()));
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,"ids",new Type("String"), "","RequestParam(value = \"ids\", required = true)"));
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		Var ids = new Var("ids",new Type("String"));
		wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
		wlist.add(NamedStatementGenerator.getFacadeCallServiceMethodByIds(2000L, 2,this.domain, service, generateServiceMethodDefinition(),ids));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndNull(3000L, 2, resultMap));
		wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName()+";"));	
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("softDeleteAll"+domain.getCapFirstPlural());
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "{"));
		sl.add(new Statement(2000,1, "text:'批软删除',"));
		sl.add(new Statement(3000,1, "iconCls:'icon-remove',"));
		sl.add(new Statement(4000,1, "handler:function(){ "));
		sl.add(new Statement(5000,2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
		sl.add(new Statement(6000,2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
		sl.add(new Statement(7000,3, "$.messager.alert(\"警告\",\"请选定记录！\",\"warning\");"));
		sl.add(new Statement(8000,3, "return;"));
		sl.add(new Statement(9000,2, "}"));
		sl.add(new Statement(10000,2, "var ids = \"\";"));
		sl.add(new Statement(11000,2, "for(var i=0;i<rows.length;i++){"));
		sl.add(new Statement(12000,3, "ids += rows[i][\""+domain.getDomainId().getLowerFirstFieldName()+"\"];"));
		sl.add(new Statement(13000,3, "if (i < rows.length-1) ids += \",\";"));
		sl.add(new Statement(14000,2, "}"));
		sl.add(new Statement(15000,2, "softDeleteAll"+domain.getCapFirstPlural()+"(ids);"));
		sl.add(new Statement(16000,1, "}"));
		sl.add(new Statement(17000,0, "}"));
		block.setMethodStatementList(sl);
		return block;			
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		Domain domain = this.domain;
		Var ids = new Var("ids",new Type("var"));
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("softDeleteAll"+StringUtil.capFirst(domain.getPlural()));
		Signature s1 = new Signature();
		s1.setName(ids.getVarName());
		s1.setPosition(1);
		s1.setType(new Type("var"));
		method.addSignature(s1);
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "$.ajax({"));
		sl.add(new Statement(2000,2, "type: \"post\","));
		sl.add(new Statement(3000,3, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/softDeleteAll"+domain.getCapFirstPlural()+"\","));
		sl.add(new Statement(4000,3, "data: {"));
		sl.add(new Statement(5000,4, "ids:ids"));
		sl.add(new Statement(6000,3, "},"));
		sl.add(new Statement(7000,3, "dataType: 'json',"));
		sl.add(new Statement(8000,3, "success: function(data, textStatus) {"));
		sl.add(new Statement(9000,4, "$(\"#dg\").datagrid(\"load\");"));
		sl.add(new Statement(10000,3, "},"));
		sl.add(new Statement(11000,3, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(12000,2, "},"));
		sl.add(new Statement(13000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(14000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(15000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(16000,2, "}"));
		sl.add(new Statement(17000,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;		
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
	}

}
