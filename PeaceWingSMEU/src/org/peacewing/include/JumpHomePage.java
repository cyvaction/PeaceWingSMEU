package org.peacewing.include;

import org.peacewing.domain.Include;
import org.peacewing.domain.StatementList;

public class JumpHomePage extends Include{
	public JumpHomePage(){
		super();
		this.fileName = "index.html";
		this.packageToken = "";
	}

	@Override
	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>\n");
		sb.append("<head>\n");
		sb.append("<meta http-equiv=\"refresh\"  content=\"0;url=pages/index.html\"/>\n");
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("</body>\n");
		return sb.toString();
	}

	@Override
	public StatementList getStatementList(long serial, int indent) {
		// TODO Auto-generated method stub
		return null;
	}

}
