package org.peacewing.oracle.oracleverb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.generator.NamedS2SMStatementGenerator;
import org.peacewing.generator.NamedS2SMStatementListGenerator;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.limitedverb.CountActiveRecords;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class ToggleOne extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception{
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception{
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception{
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception{
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception{
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("toggleOne"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,this.domain.getDomainId().getFieldName(),this.domain.getDomainId().getClassType()));
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception{
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() throws Exception{
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception{
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("toggleOne"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getDomainId().getFieldName(),this.domain.getDomainId().getClassType()));
		method.addMetaData("Override");
	
		//Service method
		FindById find = new FindById(this.domain);
		Toggle toggle = new Toggle(this.domain);
		CountActiveRecords countActive = new CountActiveRecords(this.domain);
				
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,2, this.domain.getCapFirstDomainName() + " " +this.domain.getLowerFirstDomainName() + " = dao."+StringUtil.lowerFirst(find.getVerbName())+"("+this.domain.getDomainId().getLowerFirstFieldName()+");"));
		list.add(new Statement(200L,2, "if ("+this.domain.getLowerFirstDomainName()+".get"+this.domain.getActive().getCapFirstFieldName()+"()=="+this.domain.getDomainDeletedInteger()+") {"));
		list.add(new Statement(300L,3, "dao."+StringUtil.lowerFirst(toggle.getVerbName())+"("+this.domain.getDomainId().getLowerFirstFieldName()+");"));
		list.add(new Statement(400L,2, "}"));
		list.add(new Statement(500L,2, "else {"));
		list.add(new Statement(600L,3, "Integer count = dao."+StringUtil.lowerFirst(countActive.getVerbName())+"();"));
		list.add(new Statement(700L,3, "if (count > 1){"));
		list.add(new Statement(800L,4, "dao."+StringUtil.lowerFirst(toggle.getVerbName())+"("+this.domain.getDomainId().getLowerFirstFieldName()+");"));
		list.add(new Statement(900L,3, "}"));
		list.add(new Statement(1000L,2, "}"));
		list.add(new Statement(1100L,2, "return true;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception{
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception{
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	public ToggleOne(){
		super();
		this.setLabel("留一切换");
	}
	
	public ToggleOne(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("ToggleOne"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("留一切换");
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception{
		return null;
	}

	@Override
	public Method generateFacadeMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("toggleOne"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getDomainId().getLowerFirstFieldName(),this.domain.getDomainId().getClassType(), this.domain.getPackageToken(),"RequestParam"));
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		wlist.add(NamedS2SMStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
		wlist.add(NamedStatementGenerator.getSpringMVCCallServiceMethodByDomainId(2000L, 2,this.domain, service, generateServiceMethodDefinition()));
		wlist.add(NamedS2SMStatementListGenerator.getPutJsonResultMapWithSuccessAndNull(3000L, 2, resultMap));
		wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName()+";"));	
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() throws Exception{
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception{
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("toggleOne"+domain.getCapFirstDomainName());
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "{"));
		sl.add(new Statement(2000,1, "text:'留一切换',"));
		sl.add(new Statement(3000,1, "iconCls:'icon-cut',"));
		sl.add(new Statement(4000,1, "handler:function(){ "));
		sl.add(new Statement(5000,2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
		sl.add(new Statement(6000,2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
		sl.add(new Statement(7000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(8000,3, "return;"));
		sl.add(new Statement(9000,2, "}"));
		sl.add(new Statement(10000,2, "if (rows.length > 1) {"));
		sl.add(new Statement(11000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(12000,3, "return;"));
		sl.add(new Statement(13000,2, "}"));
		sl.add(new Statement(14000,2, "var "+domain.getDomainId().getLowerFirstFieldName()+" = rows[0][\""+domain.getDomainId().getLowerFirstFieldName()+"\"];"));
		sl.add(new Statement(15000,2, "toggleOne"+this.domain.getCapFirstDomainName()+"("+domain.getDomainId().getLowerFirstFieldName()+");"));
		sl.add(new Statement(16000,1, "}"));
		sl.add(new Statement(17000,0, "}"));
		block.setMethodStatementList(sl);
		return block;		
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		Domain domain = this.domain;
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("toggleOne"+domain.getCapFirstDomainName());
		Signature s1 = new Signature();
		s1.setName(domain.getDomainId().getLowerFirstFieldName());
		s1.setPosition(1);
		s1.setType(new Type("var"));
		method.addSignature(s1);
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "$.ajax({"));
		sl.add(new Statement(2000,2, "type: \"post\","));
		sl.add(new Statement(3000,2, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/toggleOne"+domain.getCapFirstDomainName()+"\","));
		sl.add(new Statement(4000,2, "data: {"));
		sl.add(new Statement(5000,3, "\""+domain.getDomainId().getLowerFirstFieldName()+"\":"+domain.getDomainId().getLowerFirstFieldName()+""));
		sl.add(new Statement(6000,2, "},"));
		sl.add(new Statement(7000,2, "dataType: 'json',"));
		sl.add(new Statement(8000,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(9000,3, "$(\"#dg\").datagrid(\"load\");"));
		sl.add(new Statement(10000,2, "},"));
		sl.add(new Statement(11000,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(12000,2, "},"));
		sl.add(new Statement(13000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(14000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(15000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(16000,2, "}"));
		sl.add(new Statement(17000,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
	}

}
