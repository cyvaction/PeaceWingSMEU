package org.peacewing.oracle.limitedverb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.limitedverb.NoControllerVerb;
import org.peacewing.oracle.generator.MybatisOracleSqlReflector;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class CountAllPage extends NoControllerVerb {

	@Override
	public Method generateDaoImplMethod() throws Exception {		
		Method method = new Method();
		method.setStandardName("countAll"+domain.getCapFirstDomainName()+"Records");
		method.setNoContainer(true);
		List<Writeable> list = new ArrayList<Writeable>();
		Var countNum = new Var("countNum",new Type("Integer"));
		list.add(new Statement(100L,1,"<select id=\""+method.getLowerFirstMethodName()+"\" resultType=\"Integer\">"));
		list.add(new Statement(200L,2, MybatisOracleSqlReflector.generateCountRecordStatement(domain,countNum)));
		list.add(new Statement(300L,1,"</select>"));method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception{
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception{
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("countAll"+this.domain.getCapFirstDomainName()+"Records");
		method.setReturnType(new Type("Integer"));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception{
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("countAll"+StringUtil.capFirst(this.domain.getPlural())+"Page");
		method.setReturnType(new Type("Integer"));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1, "pagesize", new Type("Integer")));
		method.setThrowException(true);
		
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception{
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("countAll"+StringUtil.capFirst(this.domain.getPlural())+"Page");
		method.setReturnType(new Type("Integer"));
		method.addSignature(new Signature(1, "pagesize", new Type("Integer")));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.setThrowException(true);
		method.addMetaData("Override");
		
		Method daomethod = this.generateDaoMethodDefinition();
		
		List<Writeable> list = new ArrayList<Writeable>();
		Var pagesize = new Var("pagesize",new Type("Integer"));
		Var pagecount = new Var("pagecount",new Type("Integer"));
		Var recordcount = new Var("recordcount",new Type("Integer"));
		list.add(new Statement(1000L,2,"if ("+pagesize.getVarName()+" <= 0) "+pagesize.getVarName()+" = 10;"));
		list.add(new Statement(2000L,2,"Integer "+pagecount.getVarName()+" = 1;"));
		list.add(new Statement(3000L,2,"Integer " + recordcount.getVarName()+" = "+daomethod.generateStandardServiceImplCallString(InterVarUtil.DB.dao.getVarName())+";"));
		list.add(new Statement(4000L,2,pagecount.getVarName()+" = (int)Math.ceil((double)"+recordcount.getVarName()+"/"+pagesize.getVarName()+");"));
		list.add(new Statement(5000L,2,"if ("+pagecount.getVarName()+" <= 1)"+pagecount.getVarName()+" = 1;"));
		list.add(new Statement(6000L,2,"return "+pagecount.getVarName()+";"));
		method.setMethodStatementList(WriteableUtil.merge(list));

		return method;
	}

	@Override
	public String generateServiceImplMethodString()throws Exception{
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception{
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public CountAllPage(Domain domain){
		super();
		this.domain = domain;
		this.verbName = "countAll"+this.domain.getPlural()+"Page";
	}
	
	public CountAllPage(){
		super();
	}

}
