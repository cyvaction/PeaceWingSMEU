package org.peacewing.oracle.core;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Action;
import org.peacewing.core.SpringMVCFacade;
import org.peacewing.core.Verb;
import org.peacewing.domain.Dao;
import org.peacewing.domain.DaoImpl;
import org.peacewing.domain.Domain;
import org.peacewing.domain.ManyToMany;
import org.peacewing.domain.Method;
import org.peacewing.domain.Prism;
import org.peacewing.domain.Service;
import org.peacewing.domain.ServiceImpl;
import org.peacewing.domain.Type;
import org.peacewing.domain.ValidateInfo;
import org.peacewing.exception.ValidateException;
import org.peacewing.generator.JsonPagingGridJspTemplate;
import org.peacewing.generator.NamedUtilMethodGenerator;
import org.peacewing.limitedverb.DaoOnlyVerb;
import org.peacewing.limitedverb.NoControllerVerb;
import org.peacewing.oracle.easyui.OracleEasyUIPageTemplate;
import org.peacewing.oracle.generator.MybatisOracleDaoXmlDecorator;
import org.peacewing.oracle.limitedverb.CountActiveRecords;
import org.peacewing.oracle.limitedverb.CountAllPage;
import org.peacewing.oracle.limitedverb.CountSearchByFieldsRecords;
import org.peacewing.oracle.oracleverb.Add;
import org.peacewing.oracle.oracleverb.Delete;
import org.peacewing.oracle.oracleverb.DeleteAll;
import org.peacewing.oracle.oracleverb.FindById;
import org.peacewing.oracle.oracleverb.FindByName;
import org.peacewing.oracle.oracleverb.ListActive;
import org.peacewing.oracle.oracleverb.ListAll;
import org.peacewing.oracle.oracleverb.ListAllByPage;
import org.peacewing.oracle.oracleverb.SearchByFieldsByPage;
import org.peacewing.oracle.oracleverb.SearchByName;
import org.peacewing.oracle.oracleverb.SoftDelete;
import org.peacewing.oracle.oracleverb.SoftDeleteAll;
import org.peacewing.oracle.oracleverb.Toggle;
import org.peacewing.oracle.oracleverb.ToggleOne;
import org.peacewing.oracle.oracleverb.Update;
import org.peacewing.utils.StringUtil;

public class OraclePrism extends Prism{
	
	protected MybatisOracleDaoXmlDecorator mybatisOracleDaoXmlDecorator = new MybatisOracleDaoXmlDecorator();

	public OraclePrism() {
		super();
	}

	@Override
	public void generatePrismFiles() throws ValidateException {
		ValidateInfo info = this.validate();
		if (info.success() == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			String srcfolderPath = folderPath;
			if (this.packageToken != null && !"".equals(this.packageToken)){
				srcfolderPath = folderPath + "src/" + packagetokenToFolder(this.packageToken);

				writeToFile(srcfolderPath + "domain/" + StringUtil.capFirst(this.getDomain().getStandardName()) + ".java",
					this.getDomain().decorateCompareTo().generateClassString());
			
				if (this.getDao() != null) {
					writeToFile(
							srcfolderPath + "dao/" + StringUtil.capFirst(this.getDomain().getStandardName()) + "Dao.java",
							this.getDao().generateDaoString());
				}
	
				if (this.getDaoImpl() != null) {
					writeToFile(
							srcfolderPath + "dao/" + StringUtil.capFirst(this.getDomain().getStandardName()) + "Dao.xml",
							this.mybatisOracleDaoXmlDecorator.generateMybatisDaoXmlFileStr());
				}
	
				if (this.getService() != null) {
					writeToFile(srcfolderPath + "service/" + StringUtil.capFirst(this.getDomain().getStandardName())
							+ "Service.java", this.getService().generateServiceString());
				}
	
				if (this.getServiceImpl() != null) {
					writeToFile(srcfolderPath + "serviceimpl/" + StringUtil.capFirst(this.getDomain().getStandardName())
							+ "ServiceImpl.java", this.getServiceImpl().generateServiceImplString());
				}
	
				if (this.facade != null) {
					writeToFile(srcfolderPath + "facade/" + StringUtil.capFirst(this.domain.getCapFirstDomainName())
							+ "Facade.java", this.facade.generateFacadeString());
				}
	
				for (JsonPagingGridJspTemplate jsontp : this.jsonjsptemplates) {
					writeToFile(folderPath + "WebContent/pages/" + this.domain.getLowerFirstPlural() + ".html",
							jsontp.generateJspString());
				}
	
				for (ManyToMany mtm : this.manyToManies) {
					writeToFile(folderPath + "WebContent/pages/" + ((OracleManyToMany)mtm).getEuTemplate().getStandardName().toLowerCase() + ".html",
							mtm.getEuTemplate().generateContentString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void generatePrismFromDomain() throws ValidateException, Exception {
		if (this.domain != null) {
			if (this.getPackageToken() != null) {
				this.domain.setPackageToken(packageToken);
			}

			this.dao = new Dao();
			this.dao.setDomain(this.domain);
			this.dao.setPackageToken(this.domain.getPackageToken());
			this.daoimpl = new DaoImpl();
			this.daoimpl.setPackageToken(this.domain.getPackageToken());
			this.daoimpl.setDomain(this.domain);
			this.daoimpl.setDao(this.dao);

			this.service = new Service();
			this.service.setDomain(this.domain);
			this.service.setPackageToken(this.domain.getPackageToken());
			this.serviceimpl = new ServiceImpl(this.domain);
			this.serviceimpl.setPackageToken(this.domain.getPackageToken());
			this.serviceimpl.getDao().addAnnotation("Autowired");
			Method daoSetter = NamedUtilMethodGenerator.generateSetter("dao",
					new Type(this.domain.getCapFirstDomainName() + "Dao"));
			this.serviceimpl.addMethod(daoSetter);
			this.serviceimpl.addClassImports("org.springframework.beans.factory.annotation.Autowired");
			this.serviceimpl.setDomain(this.domain);
			this.serviceimpl.setService(this.service);

			Verb listAll = new ListAll(this.domain);
			Verb update = new Update(this.domain);
			Verb delete = new Delete(this.domain);
			Verb add = new Add(this.domain);
			Verb softdelete = new SoftDelete(this.domain);
			Verb findbyid = new FindById(this.domain);
			Verb findbyname = new FindByName(this.domain);
			Verb searchbyname = new SearchByName(this.domain);
			Verb listactive = new ListActive(this.domain);
			Verb listAllByPage = new ListAllByPage(this.domain);
			Verb deleteAll = new DeleteAll(this.domain);
			Verb softDeleteAll = new SoftDeleteAll(this.domain);
			Verb toggle = new Toggle(this.domain);
			Verb toggleOne = new ToggleOne(this.domain);
			Verb searchByFieldsByPage = new SearchByFieldsByPage(this.domain);

			CountAllPage countAllPage = new CountAllPage(this.domain);
			CountSearchByFieldsRecords countSearch = new CountSearchByFieldsRecords(this.domain);
			CountActiveRecords countActiveRecords = new CountActiveRecords(this.domain);

			this.addVerb(listAll);
			this.addVerb(update);
			this.addVerb(delete);
			this.addVerb(add);
			this.addVerb(softdelete);
			this.addVerb(findbyid);
			this.addVerb(findbyname);
			this.addVerb(searchbyname);
			this.addVerb(listactive);
			this.addVerb(listAllByPage);
			this.addVerb(deleteAll);
			this.addVerb(softDeleteAll);
			this.addVerb(toggle);
			this.addVerb(toggleOne);
			this.addVerb(searchByFieldsByPage);

			this.noControllerVerbs.add(countAllPage);
			this.noControllerVerbs.add(countSearch);
			this.noControllerVerbs.add(countActiveRecords);
			this.action = new Action(this.verbs, this.domain);
			this.action.setPackageToken(this.packageToken);
			this.facade = new SpringMVCFacade(this.verbs, this.domain);
			this.facade.setPackageToken(this.packageToken);

			for (Verb v : this.verbs) {
				v.setDomain(domain);
				service.addMethod(v.generateServiceMethodDefinition());
				serviceimpl.addMethod(v.generateServiceImplMethod());
				dao.addMethod(v.generateDaoMethodDefinition());
				daoimpl.addMethod(v.generateDaoImplMethod());
			}

			for (NoControllerVerb nVerb : this.noControllerVerbs) {
				nVerb.setDomain(domain);
				service.addMethod(nVerb.generateServiceMethodDefinition());
				serviceimpl.addMethod(nVerb.generateServiceImplMethod());
				dao.addMethod(nVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(nVerb.generateDaoImplMethod());
			}

			for (DaoOnlyVerb oVerb : this.daoOnlyVerbs) {
				oVerb.setDomain(domain);
				dao.addMethod(oVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(oVerb.generateDaoImplMethod());
			}

			this.mybatisOracleDaoXmlDecorator = new MybatisOracleDaoXmlDecorator();
			this.mybatisOracleDaoXmlDecorator.setDomain(this.getDomain());
			List<Domain> resultMaps = new ArrayList<Domain>();
			resultMaps.add(this.domain);
			this.mybatisOracleDaoXmlDecorator.setResultMaps(resultMaps);
			List<Method> daoimplMethods = new ArrayList<Method>();
			for (Verb vb : this.verbs) {
				daoimplMethods.add(vb.generateDaoImplMethod());
			}
			for (DaoOnlyVerb dovb : this.daoOnlyVerbs) {
				daoimplMethods.add(dovb.generateDaoImplMethod());
			}
			for (NoControllerVerb ncvb : this.noControllerVerbs) {
				daoimplMethods.add(ncvb.generateDaoImplMethod());
			}
			this.mybatisOracleDaoXmlDecorator.setDaoXmlMethods(daoimplMethods);

			OracleEasyUIPageTemplate easyui = new OracleEasyUIPageTemplate();
			easyui.setDomain(this.domain);
			easyui.setStandardName(this.domain.getStandardName().toLowerCase());
			this.addJsonJspTemplate(easyui);

			if (this.domain.getManyToManies() != null && this.domain.getManyToManies().size() > 0) {
				for (ManyToMany mtm : this.domain.getManyToManies()) {
					String slaveName = mtm.getManyToManySalveName();
					String masterName = this.domain.getStandardName();
					if (setContainsDomain(this.projectDomains, masterName)
							&& setContainsDomain(this.projectDomains, slaveName)) {
						this.manyToManies.add(new OracleManyToMany(lookupDoaminInSet(this.projectDomains, masterName),
								lookupDoaminInSet(this.projectDomains, slaveName),mtm.getMasterValue(),mtm.getValues()));
					} else {
						ValidateInfo validateInfo = new ValidateInfo();
						validateInfo.addCompileError("棱柱" + this.getText() + "多对多设置有误。");
						ValidateException em = new ValidateException(validateInfo);
						throw em;
					}
				}
			}
			for (ManyToMany mtm : this.manyToManies) {
				OracleManyToMany omtm = (OracleManyToMany) mtm;
				this.service.addMethod(omtm.getOracleAssign().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(omtm.getOracleAssign().generateServiceImplMethod());
				this.dao.addMethod(omtm.getOracleAssign().generateDaoMethodDefinition());
				this.daoimpl.addMethod(omtm.getOracleAssign().generateDaoImplMethod());
				this.mybatisOracleDaoXmlDecorator.addDaoXmlMethod(omtm.getOracleAssign().generateDaoImplMethod());
				this.facade.addMethod(omtm.getOracleAssign().generateFacadeMethod());

				this.service.addMethod(omtm.getOracleRevoke().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(omtm.getOracleRevoke().generateServiceImplMethod());
				this.dao.addMethod(omtm.getOracleRevoke().generateDaoMethodDefinition());
				this.daoimpl.addMethod(omtm.getOracleRevoke().generateDaoImplMethod());
				this.mybatisOracleDaoXmlDecorator.addDaoXmlMethod(omtm.getOracleRevoke().generateDaoImplMethod());
				this.facade.addMethod(omtm.getOracleRevoke().generateFacadeMethod());

				this.service.addMethod(omtm.getOracleListMyActive().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(omtm.getOracleListMyActive().generateServiceImplMethod());
				this.dao.addMethod(omtm.getOracleListMyActive().generateDaoMethodDefinition());
				this.daoimpl.addMethod(omtm.getOracleListMyActive().generateDaoImplMethod());
				this.mybatisOracleDaoXmlDecorator.addResultMap(omtm.getSlave());
				this.mybatisOracleDaoXmlDecorator.addDaoXmlMethod(omtm.getOracleListMyActive().generateDaoImplMethod());
				this.facade.addMethod(omtm.getOracleListMyActive().generateFacadeMethod());

				this.service.addMethod(omtm.getOracleListMyAvailableActive().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(omtm.getOracleListMyAvailableActive().generateServiceImplMethod());
				this.dao.addMethod(omtm.getOracleListMyAvailableActive().generateDaoMethodDefinition());
				this.daoimpl.addMethod(omtm.getOracleListMyAvailableActive().generateDaoImplMethod());
				this.mybatisOracleDaoXmlDecorator.addDaoXmlMethod(omtm.getOracleListMyAvailableActive().generateDaoImplMethod());
				this.facade.addMethod(omtm.getOracleListMyAvailableActive().generateFacadeMethod());
				omtm.getSlave().decorateCompareTo();
				
				Service slaveService = new Service();
				slaveService.setDomain(omtm.getSlave());
				slaveService.addAnnotation("Autowired");
				slaveService.setStandardName(omtm.getSlave().getCapFirstDomainName() + "Service");
				
				Method slaveServiceSetter = NamedUtilMethodGenerator.generateSetter(omtm.getSlave().getLowerFirstDomainName()+"Service",
						new Type(omtm.getSlave().getCapFirstDomainName() + "Service",mtm.getSlave().getPackageToken()+".service."+mtm.getSlave().getCapFirstDomainName() + "Service"));
				this.serviceimpl.addMethod(slaveServiceSetter);
				this.serviceimpl.addOtherService(slaveService);
			}
		}
	}

}
