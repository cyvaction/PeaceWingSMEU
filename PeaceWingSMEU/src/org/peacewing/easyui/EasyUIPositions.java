package org.peacewing.easyui;

import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;

public interface EasyUIPositions {
	public  JavascriptBlock generateEasyUIJSButtonBlock() throws Exception;
	public  String generateEasyUIJSButtonBlockString() throws Exception;
	public  String generateEasyUIJSButtonBlockStringWithSerial() throws Exception;

	public  JavascriptMethod generateEasyUIJSActionMethod() throws Exception;
	public  String generateEasyUIJSActionString() throws Exception;
	public  String generateEasyUIJSActionStringWithSerial() throws Exception;

}
