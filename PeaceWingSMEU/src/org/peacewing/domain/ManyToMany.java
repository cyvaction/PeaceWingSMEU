package org.peacewing.domain;

import java.io.Serializable;

import org.peacewing.complexverb.Assign;
import org.peacewing.complexverb.ListMyActive;
import org.peacewing.complexverb.ListMyAvailableActive;
import org.peacewing.complexverb.Revoke;
import org.peacewing.easyui.EasyUIManyToManyTemplate;
import org.peacewing.generator.MysqlTwoDomainsDBDefinitionGenerator;
import org.peacewing.utils.StringUtil;

public class ManyToMany implements Comparable<ManyToMany>, Serializable{
	private static final long serialVersionUID = -8844037024490738355L;
	protected Domain master;
	protected Domain slave;
	protected Assign assign;
	protected Revoke revoke;
	protected ListMyActive listMyActive;
	protected ListMyAvailableActive listMyAvailableActive;
	protected EasyUIManyToManyTemplate euTemplate;
	protected String standardName;
	protected String label;
	protected String values;
	protected String manyToManySalveName;
	protected String masterValue;
	
	public ManyToMany(){
		super();
	}
	
	public ManyToMany(String manyToManySalveName){
		super();
		this.manyToManySalveName = manyToManySalveName;
	}
	
	public ManyToMany(Domain master,Domain slave,String masterValue, String values){
		this.master = master;
		this.slave = slave;
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.masterValue = masterValue;
		this.values = values;
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.euTemplate = new EasyUIManyToManyTemplate(master,slave);
	}
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	public Assign getAssign() {
		return assign;
	}
	public void setAssign(Assign assign) {
		this.assign = assign;
	}
	public Revoke getRevoke() {
		return revoke;
	}
	public void setRevoke(Revoke revoke) {
		this.revoke = revoke;
	}
	public ListMyActive getListMyActive() {
		return listMyActive;
	}
	public void setListMyActive(ListMyActive listMyActive) {
		this.listMyActive = listMyActive;
	}
	public ListMyAvailableActive getListMyAvailableActive() {
		return listMyAvailableActive;
	}
	public void setListMyAvailableActive(ListMyAvailableActive listMyAvailableActive) {
		this.listMyAvailableActive = listMyAvailableActive;
	}
	public EasyUIManyToManyTemplate getEuTemplate() {
		return euTemplate;
	}
	public void setEuTemplate(EasyUIManyToManyTemplate euTemplate) {
		this.euTemplate = euTemplate;
	}
	public Domain getMaster() {
		return master;
	}
	public String getStandardName(){
		return "Link"+this.master.getStandardName()+this.slave.getStandardName();
	}
	@Override
	public int compareTo(ManyToMany o) {
		return this.getStandardName().compareTo(o.getStandardName());
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getText(){
		if (StringUtil.isBlank(this.master.getLabel())||StringUtil.isEnglishAndDigitalAndEmpty(this.master.getLabel())){
			return "Link "+this.master.getStandardName()+" "+this.slave.getStandardName();
		}
		else return "链接"+this.master.getText()+this.slave.getText();
	}
	
	public MysqlTwoDomainsDBDefinitionGenerator toTwoDBGenerator(){
		MysqlTwoDomainsDBDefinitionGenerator mtg = new MysqlTwoDomainsDBDefinitionGenerator(this.master,this.slave);
		return mtg;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getManyToManySalveName() {
		return manyToManySalveName;
	}

	public void setManyToManySalveName(String manyToManySalveName) {
		this.manyToManySalveName = manyToManySalveName;
	}

	public String getMasterValue() {
		return masterValue;
	}

	public void setMasterValue(String masterValue) {
		this.masterValue = masterValue;
	}
}
