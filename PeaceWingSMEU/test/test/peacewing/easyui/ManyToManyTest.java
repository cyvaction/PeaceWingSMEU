package test.peacewing.easyui;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.peacewing.complexverb.Assign;
import org.peacewing.complexverb.ListMyActive;
import org.peacewing.complexverb.ListMyAvailableActive;
import org.peacewing.complexverb.Revoke;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;
import org.peacewing.easyui.EasyUIManyToManyTemplate;
import org.peacewing.easyui.EasyUIPositions;

public class ManyToManyTest {
	@Test
	public void testManyToMany()  throws Exception{
		
		Domain user = getUser();
		Domain privilege = getPrivilege();
		Domain bonus = getBonus();
		Domain clockRecord = getClockRecord();		
		
		ListMyActive listMyActive = new ListMyActive(user,privilege);
		ListMyAvailableActive listMyAvailableActive = new ListMyAvailableActive(user,privilege);
		Assign assign = new Assign(user,privilege);
		Revoke revoke = new Revoke(user,privilege);
		
		List<Domain> dList = new ArrayList<Domain>();
		dList.add(user);
		dList.add(privilege);
		dList.add(bonus);
		dList.add(clockRecord);
		
		EasyUIManyToManyTemplate etp = new EasyUIManyToManyTemplate(user,privilege);
		etp.setMenuDomainList(dList);

		System.out.println("========================generateContentString===========");
		System.out.println(etp.generateContentString());
	}
	
	public Domain getUser(){
		Domain domain = new Domain();
		domain.setPackageToken("org.peacewing");
		domain.setStandardName("User");
		domain.setDomainId(new Field("id", "long"));
		domain.setDomainName(new Field("userName","String"));
		domain.setActive(new Field("active", "boolean"));
		return domain;
	}
	
	public Domain getPrivilege(){
		Domain domain = new Domain();
		domain.setPackageToken("org.peacewing");
		domain.setStandardName("Privilege");
		domain.setDomainId(new Field("id", "long"));
		domain.setDomainName(new Field("privilegeName","String"));
		domain.setActive(new Field("active", "boolean"));
		return domain;
	}
	
	public Domain getBonus(){		
		Domain domain = new Domain();
		domain.setPackageToken("org.peacewing");
		domain.setStandardName("Bonus");
		domain.setPlural("Bonuses");
		domain.addField("empid","long");
		domain.addField("userid", "long");
		domain.addField("reason", "String");
		domain.addField("bonusBalance", "double");
		domain.addField("description", "String");
		domain.setDomainId(new Field("id", "long"));
		domain.setDomainName(new Field("bonusName","String"));
		domain.setActive(new Field("active", "boolean"));
		return domain;
	}
	
	public Domain getClockRecord(){		
		Domain domain = new Domain();
		domain.setPackageToken("org.peacewing");
		domain.setStandardName("ClockRecord");
		domain.addField("empid","long");
		domain.addField("userid", "long");
		domain.addField("timeStamp", "String");
		domain.addField("description", "String");
		domain.setDomainId(new Field("id", "long"));
		domain.setDomainName(new Field("clockRecordName","String"));
		domain.setActive(new Field("active", "boolean"));
		return domain;
	}
	
}
